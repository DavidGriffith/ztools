**ZTools, the Infocom Toolkit by Mark Howell,**
**currently maintained by Matthey T.l Russotto**
================================================

Ztools contains the following tools:

- check --- checks story file integrity, same as $verify command. Works
		on all types except for V1, V2 and early V3 games which
		have no checksum in the header. Optionally outputs a new
		story file of the correct length.

- infodump --- dumps header, object list, grammar and dictionary of a
		given story file. Works on all types. Grammar doesn't
		work for V6 games yet.

- inforead --- converts IBM bootable disks to story files. Only for IBM
		PCs, requires Microsoft C or Borland C.

- pix2gif --- convert IBM MCGA/VGA (*.MG1) picture files from V6 games
		to individual GIF files, viewable on most platforms.

- txd --- disassembles story files to Z code assembly language, plus
		text strings.  Works on all games from V1 to V8. Also
		supports Inform assembler syntax.

Current version: 7/3.1 (07nov98). Some of the executables below contain
older versions of Ztools which may not support all of the game files
mentioned above.
